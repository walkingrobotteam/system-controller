# README #


### What is this repository for? ###

This folder includes the project folders for the Pneumatic Walking Robot project. These files are to be used with PSOC CREATOR 3.2. NOT TESTED WITH NEW VERSIONS OF PSOC CREATOR.


**
**### How do I get set up? ###****

After installing PSOC Creater 3.2 (available on Cypress.com) and downloading the desired project, in PSOC Creator go to file->open->project/workspace and navigate to the directory of the project you want. Click on the file with the type "PSoC Creator Workspace". The full project should open. Build the project.

Each folder was used for different setups. Please reference the following to be sure to use the correct project for what you want to do

Bluetooth Only and SD Show Demo: Project used for senior design show. IS NOT THE FULL PROJECT. This version only includes bluetooth functionality to be used by the Android Application.

Gait Controller Test Only: Debugging project used to test the Gait Controller functionality ONLY. IS NOT THE FULL PROJECT. This project will send a series of commands to the gait controller on a continuous loop.

Pneumatic Walking Robot Final Design: Project folder is what the final solution should look like. This version includes the emergency stop (never tested) and has the commands being send in the sendCommand state (never tested).

Senior Design 2015_2016 Pneumatic Walking Robot Working: This is the full project which includes working versions for both Bluetooth communication and Gait Controller communication. For this to function correctly the gait controller MUST be connected to the system controller whether by hard wire or by use of the PCB. 




### Helpful Notes ###

The full system "Senior Design 2016-2016" will not work properly unless connected to the Gait Controller. If the Gait Controller is not connected to the system controller, the program will get stuck in an infinite loop when the System controller is turned on. The System controller will continuously try to send an invalid command to the gait controller (but will be unsuccessful if not connected)

Working version shows sending command in the wait for command state. This was done as last minute resort to get the program functioning (even though this does not follow the desired design).
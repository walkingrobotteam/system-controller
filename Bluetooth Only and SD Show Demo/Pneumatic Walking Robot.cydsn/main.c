/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "BLEProcess.h"
#include "main.h"
#include "led.h"

enum STATES { init, waitForCommand, sendCommand, reset, stop } systemState;
int8 STOP, RESET, IDLE,commandReceived;
uint8 CommandData; //LENGTH SHOULD EQUAL . Sending 1
uint8 result = 0x00;
void GeneralEventHandler( uint32 eventCode, void *eventParam );
void initialize();
void sending(uint8_t cmd);
void resetSystem();
void stopSystem();
CY_ISR(EmergencyStop);

int main()
{
    CyGlobalIntEnable;   /* Enable global interrupts */
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

        //START PRISM(LED) Components
        PrISM_1_Start();
        PrISM_2_Start();
        /* Switch off the RGB LEDs through PrISM modules */
        PrISM_1_WritePulse0(RGB_LED_OFF);
         PrISM_1_WritePulse1(RGB_LED_OFF);
         PrISM_2_WritePulse0(RGB_LED_OFF);
         /* Set Drive modes of the output pins to Strong drive */
        RED_SetDriveMode(RED_DM_STRONG);
        GREEN_SetDriveMode(GREEN_DM_STRONG);
         BLUE_SetDriveMode(BLUE_DM_STRONG);
    systemState = init;
    
    
    
    for(;;)
    {
        /* Place your application code here */
        //CyBle_ProcessEvents();
        switch(systemState){
            case init:
            initialize();
            systemState = waitForCommand;
            break;
            
            case waitForCommand:
                /*PrISM_1_WritePulse0(0x00);
                PrISM_1_WritePulse1(0x00);
                PrISM_2_WritePulse0(RGB_LED_MAX_VAL);*/
            
            
            while(commandReceived == 0){
            CyBle_ProcessEvents();
            if(STOP == 1){ //check first
                systemState = stop; 
                break;
            }
            else if(RESET == 1){ //chec 
                systemState = reset;
                break;
            }
        }
                if(commandReceived == 1){
                    systemState = sendCommand;
                }
                
            
            break;
            
            case sendCommand:
                //do stuff first
                //sending(CommandData);
                //send the command to the MEGA
                //change the command Received back to 0
                commandReceived = 0;
                //wait for another command 
                systemState = waitForCommand; //for Now just go back to waiting for a command
            break;
            
            case reset:
            resetSystem();
            break;
            
            case stop:
            stopSystem();
            break; 
        }
    }
}


void GeneralEventHandler(uint32 event, void * eventParam)
      {
        /* Structure to store data written by Client */
        CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;
        /* 'CommandData[]' is an array to store 1 bytes of Commans data*/
            //Color settings below show that the system has successfully entered the waiting state
            PrISM_1_WritePulse0(0X00);
            PrISM_1_WritePulse1(0x00);
            PrISM_2_WritePulse0(0XFF);
            switch(event)
             {
            case CYBLE_EVT_STACK_ON:
            /* This event is generated when BLE stack is ON */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            break;

            
            
            
            case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
                /* This event is generated whenever Advertisement starts or stops */
            if(CyBle_GetState() == CYBLE_STATE_DISCONNECTED)
            {
                CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
             }
            break;
            
            
            
            
            case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            /* This event is generated at GAP disconnection. */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            systemState = waitForCommand;
            break;
            
            
            
            
             case CYBLE_EVT_GATTS_WRITE_REQ:
            /* This event is generated when the connected Central */
            /* device sends a Write request. */
            /* The parameter ‘eventParam’ contains the data written */
            
            /* Extract the Write data sent by Client */
            wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T *) eventParam;
            
            
            /* If the attribute handle of the characteristic written to * is equal to that of Robot Command characteristic, then extract * command data */ 
            if(CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE == wrReqParam->handleValPair.attrHandle) 
            { 
                //commandReceived = 1; // a command is received
                
                
                //should store DATA into command variable.
                //should check to see whether command is stop or reset,
                //if stop or reset, then set STOP = 1 or RESET = 1 respectively
                //otherwise store data and move state to send command state
                /* Store RGB LED data in local array */
                CommandData = wrReqParam->handleValPair.value.val[0]; //data stored at index 0
                /*if(CommandData == 0x04){ //stop command
                    STOP = 1; //stop the system by going into stop state
                }
                if(CommandData == 0x03){ //reset command
                    RESET = 1;//reset the system by going into reset state
                }*/
                
                /* Update the GATT DB for Robot Command read characteristics*/
                UpdateRobotCommandcharacteristic(&CommandData, sizeof(CommandData), CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE );
                    
                    if(CommandData == 0x11){ //BLUE  //turn right
                    PrISM_1_WritePulse0(0XFF);
                    PrISM_1_WritePulse1(0xFF);
                    PrISM_2_WritePulse0(0X00);
                    //CyDelay(2000);
                    }
                    if(CommandData == 0x12){ //RED //turn left
                    PrISM_1_WritePulse0(0X00);
                    PrISM_1_WritePulse1(0xFF);
                    PrISM_2_WritePulse0(0XFF);
                    //CyDelay(2000);
                    }
                    if(CommandData == 0x13){ //GREEN/BLUE/RED  //walk forward
                    PrISM_1_WritePulse0(0XFF);
                    PrISM_1_WritePulse1(0x00);
                    PrISM_2_WritePulse0(0XFF);
                    //CyDelay(2000);
                    }
                    if(CommandData == 0x14){ //PURPLE walk backwards
                    PrISM_1_WritePulse0(0X00);
                    PrISM_1_WritePulse1(0xFF);
                    PrISM_2_WritePulse0(0X00);
                    }
                    if(CommandData == 0x15){ // GREEN& BLUE STOP
                    PrISM_1_WritePulse0(0xFF);
                    PrISM_1_WritePulse1(0x00);
                    PrISM_2_WritePulse0(0X00);
                    systemState = stop;
                    break;
                    }
                    if(CommandData == 0x16){ // GREEN& BLUE RESET
                    PrISM_1_WritePulse0(0x00);
                    PrISM_1_WritePulse1(0x0F);
                    PrISM_2_WritePulse0(0X00);
                    }
            }
            
            
            /* Send the response to the write request received. */
            CyBle_GattsWriteRsp(cyBle_connHandle);
            
            
            break;
            
            

            case CYBLE_EVT_GATT_DISCONNECT_IND:
            /* This event is generated at GATT disconnection. */
            
            /* Reset the color values*/
            CommandData = FALSE;
                PrISM_1_WritePulse0(0X00);
                PrISM_1_WritePulse1(0x00);
                PrISM_2_WritePulse0(0X00);
            
            
            /* Switch off LEDs */
            //UpdateRGBLED(RGBledData, sizeof(RGBledData));
            //SHOW WAITING FOR CONNECTION ON LED's
            
            /* Register the new color in GATT DB*/
            UpdateRobotCommandcharacteristic(&CommandData,
            sizeof(CommandData),
            CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE);
            systemState = waitForCommand; //return to waiting state
            break;
            
            
             default:
             break;
            }
      }
void initialize(){
    //turn on LED
    PrISM_1_WritePulse0(0xFF);
    PrISM_1_WritePulse1(0x00);
    PrISM_2_WritePulse0(0X00);
    CyDelay(1500);
    E_Stop_Start(); //not needed for demo application
    E_Stop_StartEx(EmergencyStop); //not needed for demo application
    CyGlobalIntEnable; //enable interrupts
    CyBle_Start(GeneralEventHandler);
    
    //reset RESET and STOP
    RESET = 0;
    STOP = 0;
    
    //turn on LED
    PrISM_1_WritePulse0(0X00);
    PrISM_1_WritePulse1(0XFF);
    PrISM_2_WritePulse0(0X00);

    
}

void resetSystem(){
    //CyGlobalIntEnable; //enable interrupts
    CyBle_SoftReset();
    systemState = init;
}
void stopSystem(){
    CyGlobalIntDisable; //Disable interrupts
    CyBle_Stop();
    //turn off LEDs
    PrISM_1_WritePulse0(RGB_LED_OFF);
    PrISM_1_WritePulse1(RGB_LED_OFF);
    PrISM_2_WritePulse0(RGB_LED_OFF);
    
}

//emergency stop ISR. Not needed for demo application
CY_ISR(EmergencyStop){
    //return pin to 0
    EStopIn_Write(0);
    //set STOP flag to 1
    STOP = 1;
    //move state to STOP state
    systemState = stop;
}

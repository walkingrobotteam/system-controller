/*******************************************************************************
* File Name: PSoC_SPI.h
* Version 2.70
*
* Description:
*  Contains the function prototypes, constants and register definition
*  of the SPI Slave Component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SPIS_PSoC_SPI_H)
#define CY_SPIS_PSoC_SPI_H

#include "cytypes.h"
#include "CyLib.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component SPI_Slave_v2_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */


/***************************************
*   Conditional Compilation Parameters
***************************************/

#define PSoC_SPI_DATA_WIDTH                  (8u)
#define PSoC_SPI_INTERNAL_TX_INT_ENABLED     (0u)
#define PSoC_SPI_INTERNAL_RX_INT_ENABLED     (0u)
#define PSoC_SPI_MODE_USE_ZERO               (1u)
#define PSoC_SPI_BIDIRECTIONAL_MODE          (1u)
#define PSoC_SPI_MODE                        (0u)

#define PSoC_SPI_FIFO_SIZE                  (4u)
/* Internal interrupt handling */
#define PSoC_SPI_TX_BUFFER_SIZE             (4u)
#define PSoC_SPI_RX_BUFFER_SIZE             (4u)
#define PSoC_SPI_INTERNAL_TX_INT_ENABLED    (0u)
#define PSoC_SPI_INTERNAL_RX_INT_ENABLED    (0u)

#define PSoC_SPI_TX_SOFTWARE_BUF_ENABLED    ((0u != PSoC_SPI_INTERNAL_TX_INT_ENABLED) && \
                                                     (PSoC_SPI_TX_BUFFER_SIZE > PSoC_SPI_FIFO_SIZE))

#define PSoC_SPI_RX_SOFTWARE_BUF_ENABLED    ((0u != PSoC_SPI_INTERNAL_RX_INT_ENABLED) && \
                                                     (PSoC_SPI_RX_BUFFER_SIZE > PSoC_SPI_FIFO_SIZE))


/***************************************
*        Data Struct Definition
***************************************/

/* Sleep Mode API Support */
typedef struct
{
    uint8 enableState;
    uint8 cntrPeriod;
} PSoC_SPI_BACKUP_STRUCT;


/***************************************
*        Function Prototypes
***************************************/

void  PSoC_SPI_Init(void) ;
void  PSoC_SPI_Enable(void) ;
void  PSoC_SPI_Start(void) ;
void  PSoC_SPI_Stop(void) ;
void  PSoC_SPI_EnableTxInt(void) ;
void  PSoC_SPI_EnableRxInt(void) ;
void  PSoC_SPI_DisableTxInt(void) ;
void  PSoC_SPI_DisableRxInt(void) ;
void  PSoC_SPI_SetTxInterruptMode(uint8 intSrc) ;
void  PSoC_SPI_SetRxInterruptMode(uint8 intSrc) ;
uint8 PSoC_SPI_ReadTxStatus(void) ;
uint8 PSoC_SPI_ReadRxStatus(void) ;
void  PSoC_SPI_WriteTxData(uint8 txData);

#if(PSoC_SPI_MODE_USE_ZERO != 0u)
    void  PSoC_SPI_WriteTxDataZero(uint8 txDataByte) \
                                              ;
#endif /* (PSoC_SPI_MODE_USE_ZERO != 0u) */

uint8 PSoC_SPI_ReadRxData(void) ;
uint8 PSoC_SPI_GetRxBufferSize(void) ;
uint8 PSoC_SPI_GetTxBufferSize(void) ;
void  PSoC_SPI_ClearRxBuffer(void) ;
void  PSoC_SPI_ClearTxBuffer(void) ;

#if (PSoC_SPI_BIDIRECTIONAL_MODE != 0u)
    void  PSoC_SPI_TxEnable(void) ;
    void  PSoC_SPI_TxDisable(void) ;
#endif /* PSoC_SPI_BIDIRECTIONAL_MODE != 0u */

void  PSoC_SPI_PutArray(const uint8 buffer[], uint8 byteCount) ;
void  PSoC_SPI_ClearFIFO(void) ;
void  PSoC_SPI_Sleep(void) ;
void  PSoC_SPI_Wakeup(void) ;
void  PSoC_SPI_SaveConfig(void) ;
void  PSoC_SPI_RestoreConfig(void) ;

/* Communication bootloader APIs */
#if defined(CYDEV_BOOTLOADER_IO_COMP) && ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_PSoC_SPI) || \
                                          (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))
    /* Physical layer functions */
    void    PSoC_SPI_CyBtldrCommStart(void) CYSMALL ;
    void    PSoC_SPI_CyBtldrCommStop(void) CYSMALL ;
    void    PSoC_SPI_CyBtldrCommReset(void) CYSMALL ;
    cystatus PSoC_SPI_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut) CYSMALL
             ;
    cystatus PSoC_SPI_CyBtldrCommRead(uint8 pData[], uint16 size, uint16 * count, uint8 timeOut) CYSMALL
             ;

    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_PSoC_SPI)
        #define CyBtldrCommStart    PSoC_SPI_CyBtldrCommStart
        #define CyBtldrCommStop     PSoC_SPI_CyBtldrCommStop
        #define CyBtldrCommReset    PSoC_SPI_CyBtldrCommReset
        #define CyBtldrCommWrite    PSoC_SPI_CyBtldrCommWrite
        #define CyBtldrCommRead     PSoC_SPI_CyBtldrCommRead
    #endif  /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_PSoC_SPI) */

    /* Byte to Byte time out for detecting end of block data from host */
    #define PSoC_SPI_BYTE2BYTE_TIME_OUT (1u)

#endif /* (CYDEV_BOOTLOADER_IO_COMP) && ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_PSoC_SPI) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface)) */


CY_ISR_PROTO(PSoC_SPI_TX_ISR);
CY_ISR_PROTO(PSoC_SPI_RX_ISR);

/* Macros for getting software status of SPIS Statusi Register */
#define PSoC_SPI_GET_STATUS_TX(swTxSts) ( (uint8)(PSoC_SPI_TX_STATUS_REG | \
                                                       ((swTxSts) & PSoC_SPI_STS_CLR_ON_RD_BYTES_MASK)) )
#define PSoC_SPI_GET_STATUS_RX(swRxSts) ( (uint8)(PSoC_SPI_RX_STATUS_REG | \
                                                       ((swRxSts) & PSoC_SPI_STS_CLR_ON_RD_BYTES_MASK)) )


/***************************************
*   Variable with external linkage
***************************************/

extern uint8 PSoC_SPI_initVar;


/***************************************
*           API Constants
***************************************/

#define PSoC_SPI_TX_ISR_NUMBER     ((uint8)PSoC_SPI_TxInternalInterrupt__INTC_NUMBER)
#define PSoC_SPI_RX_ISR_NUMBER     ((uint8)PSoC_SPI_RxInternalInterrupt__INTC_NUMBER)
#define PSoC_SPI_TX_ISR_PRIORITY   ((uint8)PSoC_SPI_TxInternalInterrupt__INTC_PRIOR_NUM)
#define PSoC_SPI_RX_ISR_PRIORITY   ((uint8)PSoC_SPI_RxInternalInterrupt__INTC_PRIOR_NUM)


/***************************************
*    Initial Parameter Constants
***************************************/

#define PSoC_SPI_INT_ON_SPI_DONE    (uint8)(0u << PSoC_SPI_STS_SPI_DONE_SHIFT)
#define PSoC_SPI_INT_ON_TX_EMPTY    (uint8)(0u << PSoC_SPI_STS_TX_FIFO_EMPTY_SHIFT)
#define PSoC_SPI_INT_ON_TX_NOT_FULL (uint8)(0u << PSoC_SPI_STS_TX_FIFO_NOT_FULL_SHIFT)
#define PSoC_SPI_INT_ON_BYTE_COMP   (uint8)(0u << PSoC_SPI_STS_BYTE_COMPLETE_SHIFT)

#define PSoC_SPI_TX_INIT_INTERRUPTS_MASK  (PSoC_SPI_INT_ON_SPI_DONE | \
                                            PSoC_SPI_INT_ON_TX_EMPTY | PSoC_SPI_INT_ON_TX_NOT_FULL | \
                                            PSoC_SPI_INT_ON_BYTE_COMP)

#define PSoC_SPI_INT_ON_RX_EMPTY     (uint8)(0u << PSoC_SPI_STS_RX_FIFO_EMPTY_SHIFT)
#define PSoC_SPI_INT_ON_RX_NOT_EMPTY (uint8)(0u << PSoC_SPI_STS_RX_FIFO_NOT_EMPTY_SHIFT)
#define PSoC_SPI_INT_ON_RX_OVER      (uint8)(0u << PSoC_SPI_STS_RX_FIFO_OVERRUN_SHIFT)
#define PSoC_SPI_INT_ON_RX_FULL      (uint8)(0u << PSoC_SPI_STS_RX_FIFO_FULL_SHIFT)

#define PSoC_SPI_RX_INIT_INTERRUPTS_MASK (PSoC_SPI_INT_ON_RX_EMPTY | \
                                            PSoC_SPI_INT_ON_RX_NOT_EMPTY | PSoC_SPI_INT_ON_RX_OVER | \
                                            PSoC_SPI_INT_ON_RX_FULL)

#define PSoC_SPI_BITCTR_INIT           (PSoC_SPI_DATA_WIDTH - 1u)

#define PSoC_SPI__MODE_00 0
#define PSoC_SPI__MODE_01 1
#define PSoC_SPI__MODE_10 2
#define PSoC_SPI__MODE_11 3


#define PSoC_SPI_TX_BUFFER_SIZE         (4u)
#define PSoC_SPI_RX_BUFFER_SIZE         (4u)

/* Following definitions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects
*/
#define PSoC_SPI_INIT_INTERRUPTS_MASK  (PSoC_SPI_INT_ON_SPI_DONE | PSoC_SPI_INT_ON_TX_EMPTY | \
                                            PSoC_SPI_INT_ON_TX_NOT_FULL | PSoC_SPI_INT_ON_RX_EMPTY | \
                                            PSoC_SPI_INT_ON_RX_NOT_EMPTY | PSoC_SPI_INT_ON_RX_OVER | \
                                            PSoC_SPI_INT_ON_BYTE_COMP)


/***************************************
*             Registers
***************************************/
#if(CY_PSOC3 || CY_PSOC5)
    #define PSoC_SPI_TXDATA_ZERO_REG          (* (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

    #define PSoC_SPI_TXDATA_ZERO_PTR           (  (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

    #define PSoC_SPI_RXDATA_ZERO_REG           (* (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

    #define PSoC_SPI_RXDATA_ZERO_PTR           (  (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

    #define PSoC_SPI_TXDATA_REG                (* (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__F0_REG)

    #define PSoC_SPI_TXDATA_PTR                (  (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__F0_REG)

    #define PSoC_SPI_RXDATA_REG                (* (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__F1_REG)

    #define PSoC_SPI_RXDATA_PTR                (  (reg8  *) \
            PSoC_SPI_BSPIS_sR8_Dp_u0__F1_REG)
#else
    #if(PSoC_SPI_DATA_WIDTH <= 8u) /* 8bit - SPIS */
        #define PSoC_SPI_TXDATA_ZERO_REG           (* (reg8 *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

        #define PSoC_SPI_TXDATA_ZERO_PTR           (  (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

        #define PSoC_SPI_RXDATA_ZERO_REG           (* (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

        #define PSoC_SPI_RXDATA_ZERO_PTR           (  (reg8 *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__A0_REG)

        #define PSoC_SPI_TXDATA_REG                (* (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__F0_REG)

        #define PSoC_SPI_TXDATA_PTR                (  (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__F0_REG)

        #define PSoC_SPI_RXDATA_REG                (* (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__F1_REG)

        #define PSoC_SPI_RXDATA_PTR                (  (reg8  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__F1_REG)
    #else /* 16bit - SPIS */
        #define PSoC_SPI_TXDATA_ZERO_REG           (* (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_A0_REG)

        #define PSoC_SPI_TXDATA_ZERO_PTR           (  (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_A0_REG)

        #define PSoC_SPI_RXDATA_ZERO_REG           (* (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_A0_REG)

        #define PSoC_SPI_RXDATA_ZERO_PTR           (  (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_A0_REG)

        #define PSoC_SPI_TXDATA_REG                (* (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_F0_REG)

        #define PSoC_SPI_TXDATA_PTR                (  (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_F0_REG)

        #define PSoC_SPI_RXDATA_REG                (* (reg16  *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_F1_REG)

        #define PSoC_SPI_RXDATA_PTR                (  (reg16 *) \
                PSoC_SPI_BSPIS_sR8_Dp_u0__16BIT_F1_REG)
    #endif /* (PSoC_SPI_DATA_WIDTH <= 8u) */
#endif     /* (CY_PSOC3 || CY_PSOC5) */

#define PSoC_SPI_TX_AUX_CONTROL_DP0_REG       (* (reg8 *) \
        PSoC_SPI_BSPIS_sR8_Dp_u0__DP_AUX_CTL_REG)
#define PSoC_SPI_TX_AUX_CONTROL_DP0_PTR       (  (reg8 *) \
        PSoC_SPI_BSPIS_sR8_Dp_u0__DP_AUX_CTL_REG)

#define PSoC_SPI_RX_AUX_CONTROL_DP0_REG       (* (reg8 *) \
        PSoC_SPI_BSPIS_sR8_Dp_u0__DP_AUX_CTL_REG)
#define PSoC_SPI_RX_AUX_CONTROL_DP0_PTR       (  (reg8 *) \
        PSoC_SPI_BSPIS_sR8_Dp_u0__DP_AUX_CTL_REG)

#if(PSoC_SPI_DATA_WIDTH > 8u)

    #define PSoC_SPI_TX_AUX_CONTROL_DP1_REG   (* (reg8 *) \
            PSoC_SPI_BSPIS_sR8_Dp_u1__DP_AUX_CTL_REG)
    #define PSoC_SPI_TX_AUX_CONTROL_DP1_PTR   (  (reg8 *) \
            PSoC_SPI_BSPIS_sR8_Dp_u1__DP_AUX_CTL_REG)

    #define PSoC_SPI_RX_AUX_CONTROL_DP1_REG   (* (reg8 *) \
            PSoC_SPI_BSPIS_sR8_Dp_u1__DP_AUX_CTL_REG)
    #define PSoC_SPI_RX_AUX_CONTROL_DP1_PTR   (  (reg8 *) \
            PSoC_SPI_BSPIS_sR8_Dp_u1__DP_AUX_CTL_REG)

#endif /* PSoC_SPI_DATA_WIDTH > 8u */


#define PSoC_SPI_COUNTER_PERIOD_REG    (* (reg8 *) \
        PSoC_SPI_BSPIS_BitCounter__PERIOD_REG)
#define PSoC_SPI_COUNTER_PERIOD_PTR    (  (reg8 *) \
        PSoC_SPI_BSPIS_BitCounter__PERIOD_REG)

#define PSoC_SPI_TX_STATUS_MASK_REG    (* (reg8 *) \
        PSoC_SPI_BSPIS_TxStsReg__MASK_REG)
#define PSoC_SPI_TX_STATUS_MASK_PTR    (  (reg8 *) \
        PSoC_SPI_BSPIS_TxStsReg__MASK_REG)

#define PSoC_SPI_RX_STATUS_MASK_REG    (* (reg8 *) \
        PSoC_SPI_BSPIS_RxStsReg__MASK_REG)
#define PSoC_SPI_RX_STATUS_MASK_PTR    (  (reg8 *) \
        PSoC_SPI_BSPIS_RxStsReg__MASK_REG)

#define PSoC_SPI_ONE_REG               (* (reg8 *) \
        PSoC_SPI_BSPIS_SPISlave_dpCounter_u0__D1_REG)
#define PSoC_SPI_ONE_PTR               (  (reg8 *) \
        PSoC_SPI_BSPIS_dpCounter_u0__D1_REG)

#define PSoC_SPI_TX_STATUS_REG         (* (reg8 *) \
        PSoC_SPI_BSPIS_TxStsReg__STATUS_REG)
#define PSoC_SPI_TX_STATUS_PTR         (  (reg8 *) \
        PSoC_SPI_BSPIS__TxStsReg__STATUS_REG)

#define PSoC_SPI_RX_STATUS_REG         (* (reg8 *) \
        PSoC_SPI_BSPIS_RxStsReg__STATUS_REG)
#define PSoC_SPI_RX_STATUS_PTR         (  (reg8 *) \
        PSoC_SPI_BSPIS_RxStsReg__STATUS_REG)

#define PSoC_SPI_COUNTER_CONTROL_REG   (* (reg8 *) \
        PSoC_SPI_BSPIS_BitCounter__CONTROL_AUX_CTL_REG)
#define PSoC_SPI_COUNTER_CONTROL_PTR   (  (reg8 *) \
        PSoC_SPI_BSPIS_BitCounter__CONTROL_AUX_CTL_REG)

#define PSoC_SPI_TX_STATUS_ACTL_REG    (* (reg8 *) \
        PSoC_SPI_BSPIS_TxStsReg__STATUS_AUX_CTL_REG)
#define PSoC_SPI_TX_STATUS_ACTL_PTR    (  (reg8 *) \
        PSoC_SPI_TX_BSPIS_TxStsReg__STATUS_AUX_CTL_REG)

#define PSoC_SPI_RX_STATUS_ACTL_REG    (* (reg8 *) \
        PSoC_SPI_BSPIS_RxStsReg__STATUS_AUX_CTL_REG)
#define PSoC_SPI_RX_STATUS_ACTL_PTR    (  (reg8 *) \
        PSoC_SPI_RX_BSPIS_RxStsReg__STATUS_AUX_CTL_REG)

#if(PSoC_SPI_BIDIRECTIONAL_MODE)

    #define PSoC_SPI_CONTROL_REG       (* (reg8 *) \
   PSoC_SPI_BSPIS_SyncCtl_CtrlReg__CONTROL_REG)
    #define PSoC_SPI_CONTROL_PTR       (  (reg8 *) \
   PSoC_SPI_BSPIS_SyncCtl_CtrlReg__CONTROL_REG)

#endif /* PSoC_SPI_BIDIRECTIONAL_MODE */


/***************************************
*       Register Constants
***************************************/

/* Status Register Definitions */
#define PSoC_SPI_STS_SPI_DONE_SHIFT             (0x00u)
#define PSoC_SPI_STS_TX_FIFO_NOT_FULL_SHIFT     (0x01u)
#define PSoC_SPI_STS_TX_FIFO_EMPTY_SHIFT        (0x02u)
#define PSoC_SPI_STS_RX_FIFO_NOT_EMPTY_SHIFT    (0x03u)
#define PSoC_SPI_STS_RX_FIFO_EMPTY_SHIFT        (0x04u)
#define PSoC_SPI_STS_RX_FIFO_OVERRUN_SHIFT      (0x05u)
#define PSoC_SPI_STS_RX_FIFO_FULL_SHIFT         (0x06u)
#define PSoC_SPI_STS_BYTE_COMPLETE_SHIFT        (0x06u)

#define PSoC_SPI_STS_SPI_DONE                   ((uint8)(0x01u << PSoC_SPI_STS_SPI_DONE_SHIFT))
#define PSoC_SPI_STS_TX_FIFO_EMPTY              ((uint8)(0x01u << PSoC_SPI_STS_TX_FIFO_EMPTY_SHIFT))
#define PSoC_SPI_STS_TX_FIFO_NOT_FULL           ((uint8)(0x01u << PSoC_SPI_STS_TX_FIFO_NOT_FULL_SHIFT))
#define PSoC_SPI_STS_RX_FIFO_EMPTY              ((uint8)(0x01u << PSoC_SPI_STS_RX_FIFO_EMPTY_SHIFT))
#define PSoC_SPI_STS_RX_FIFO_NOT_EMPTY          ((uint8)(0x01u << PSoC_SPI_STS_RX_FIFO_NOT_EMPTY_SHIFT))
#define PSoC_SPI_STS_RX_FIFO_OVERRUN            ((uint8)(0x01u << PSoC_SPI_STS_RX_FIFO_OVERRUN_SHIFT))
#define PSoC_SPI_STS_RX_FIFO_FULL               ((uint8)(0x01u << PSoC_SPI_STS_RX_FIFO_FULL_SHIFT))
#define PSoC_SPI_STS_BYTE_COMPLETE              ((uint8)(0x01u << PSoC_SPI_STS_BYTE_COMPLETE_SHIFT))

#define PSoC_SPI_STS_CLR_ON_RD_BYTES_MASK       (0x61u)

/* StatusI Register Interrupt Enable Control Bits */
/* As defined by the Register map for the AUX Control Register */
#define PSoC_SPI_INT_ENABLE                     (0x10u)
#define PSoC_SPI_TX_FIFO_CLR    (0x01u) /* F0 - TX FIFO */
#define PSoC_SPI_RX_FIFO_CLR    (0x02u) /* F1 - RX FIFO */
#define PSoC_SPI_FIFO_CLR       (PSoC_SPI_TX_FIFO_CLR | PSoC_SPI_RX_FIFO_CLR)

/* Bit Counter (7-bit) Control Register Bit Definitions */
/* As defined by the Register map for the AUX Control Register */
#define PSoC_SPI_CNTR_ENABLE                    (0x20u)

/* Bi-Directional mode control bit */
#define PSoC_SPI_CTRL_TX_SIGNAL_EN              (0x01u)

/* Datapath Auxillary Control Register definitions */
#define PSoC_SPI_AUX_CTRL_FIFO0_CLR             (0x00u)
#define PSoC_SPI_AUX_CTRL_FIFO1_CLR             (0x00u)
#define PSoC_SPI_AUX_CTRL_FIFO0_LVL             (0x00u)
#define PSoC_SPI_AUX_CTRL_FIFO1_LVL             (0x00u)
#define PSoC_SPI_STATUS_ACTL_INT_EN_MASK        (0x10u)

/* Component disabled */
#define PSoC_SPI_DISABLED   (0u)

/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define PSoC_SPI_TXDATA_ZERO               (PSoC_SPI_TXDATA_ZERO_REG)
#define PSoC_SPI_TXDATA                    (PSoC_SPI_TXDATA_REG)
#define PSoC_SPI_RXDATA                    (PSoC_SPI_RXDATA_REG)
#define PSoC_SPI_MISO_AUX_CONTROLDP0       (PSoC_SPI_MISO_AUX_CTRL_DP0_REG)
#define PSoC_SPI_MOSI_AUX_CONTROLDP0       (PSoC_SPI_MOSI_AUX_CTRL_DP0_REG)
#define PSoC_SPI_TXBUFFERREAD              (PSoC_SPI_txBufferRead)
#define PSoC_SPI_TXBUFFERWRITE             (PSoC_SPI_txBufferWrite)
#define PSoC_SPI_RXBUFFERREAD              (PSoC_SPI_rxBufferRead)
#define PSoC_SPI_RXBUFFERWRITE             (PSoC_SPI_rxBufferWrite)

#if(PSoC_SPI_DATA_WIDTH > 8u)

    #define PSoC_SPI_MISO_AUX_CONTROLDP1   (PSoC_SPI_MISO_AUX_CTRL_DP1_REG)
    #define PSoC_SPI_MOSI_AUX_CONTROLDP1   (PSoC_SPI_MOSI_AUX_CTRL_DP0_REG)

#endif /* PSoC_SPI_DATA_WIDTH > 8u */

#define PSoC_SPI_COUNTER_PERIOD            (PSoC_SPI_COUNTER_PERIOD_REG)
#define PSoC_SPI_COUNTER_CONTROL           (PSoC_SPI_COUNTER_CONTROL_REG)
#define PSoC_SPI_ONE                       (PSoC_SPI_ONE_REG)
#define PSoC_SPI_STATUS                    (PSoC_SPI_TX_STATUS_REG)
#define PSoC_SPI_STATUS_MASK               (PSoC_SPI_TX_STATUS_MASK_REG)
#define PSoC_SPI_STATUS_ACTL               (PSoC_SPI_TX_STATUS_ACTL_REG)

#define PSoC_SPI_WriteByte      (PSoC_SPI_WriteTxData)
#define PSoC_SPI_ReadByte       (PSoC_SPI_ReadRxData)
#define PSoC_SPI_WriteByteZero  (PSoC_SPI_WriteTxDataZero)
void  PSoC_SPI_SetInterruptMode(uint8 intSrc) ;
uint8 PSoC_SPI_ReadStatus(void) ;
void  PSoC_SPI_EnableInt(void) ;
void  PSoC_SPI_DisableInt(void) ;

#define PSoC_SPI_STS_TX_BUF_NOT_FULL_SHIFT      (0x01u)
#define PSoC_SPI_STS_TX_BUF_FULL_SHIFT          (0x02u)
#define PSoC_SPI_STS_RX_BUF_NOT_EMPTY_SHIFT     (0x03u)
#define PSoC_SPI_STS_RX_BUF_EMPTY_SHIFT         (0x04u)
#define PSoC_SPI_STS_RX_BUF_OVERRUN_SHIFT       (0x05u)

#define PSoC_SPI_STS_TX_BUF_NOT_FULL            ((uint8)(0x01u << PSoC_SPI_STS_TX_BUF_NOT_FULL_SHIFT))
#define PSoC_SPI_STS_TX_BUF_FULL                ((uint8)(0x01u << PSoC_SPI_STS_TX_BUF_FULL_SHIFT))
#define PSoC_SPI_STS_RX_BUF_NOT_EMPTY           ((uint8)(0x01u << PSoC_SPI_STS_RX_BUF_NOT_EMPTY_SHIFT))
#define PSoC_SPI_STS_RX_BUF_EMPTY               ((uint8)(0x01u << PSoC_SPI_STS_RX_BUF_EMPTY_SHIFT))
#define PSoC_SPI_STS_RX_BUF_OVERRUN             ((uint8)(0x01u << PSoC_SPI_STS_RX_BUF_OVERRUN_SHIFT))

#define PSoC_SPI_DataWidth                  (PSoC_SPI_DATA_WIDTH)
#define PSoC_SPI_InternalClockUsed          (PSoC_SPI_INTERNAL_CLOCK)
#define PSoC_SPI_InternalTxInterruptEnabled (PSoC_SPI_INTERNAL_TX_INT_ENABLED)
#define PSoC_SPI_InternalRxInterruptEnabled (PSoC_SPI_INTERNAL_RX_INT_ENABLED)
#define PSoC_SPI_ModeUseZero                (PSoC_SPI_MODE_USE_ZERO)
#define PSoC_SPI_BidirectionalMode          (PSoC_SPI_BIDIRECTIONAL_MODE)
#define PSoC_SPI_Mode                       (PSoC_SPI_MODE)
#define PSoC_SPI_DATAWIDHT                  (PSoC_SPI_DATA_WIDTH)
#define PSoC_SPI_InternalInterruptEnabled   (0u)

#define PSoC_SPI_TXBUFFERSIZE   (PSoC_SPI_TX_BUFFER_SIZE)
#define PSoC_SPI_RXBUFFERSIZE   (PSoC_SPI_RX_BUFFER_SIZE)

#define PSoC_SPI_TXBUFFER       PSoC_SPI_txBuffer
#define PSoC_SPI_RXBUFFER       PSoC_SPI_rxBuffer

#endif  /* CY_SPIS_PSoC_SPI_H */

/* [] END OF FILE */

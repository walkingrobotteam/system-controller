/*******************************************************************************
* File Name: PSoC_SPI_IntClock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_PSoC_SPI_IntClock_H)
#define CY_CLOCK_PSoC_SPI_IntClock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void PSoC_SPI_IntClock_StartEx(uint32 alignClkDiv);
#define PSoC_SPI_IntClock_Start() \
    PSoC_SPI_IntClock_StartEx(PSoC_SPI_IntClock__PA_DIV_ID)

#else

void PSoC_SPI_IntClock_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void PSoC_SPI_IntClock_Stop(void);

void PSoC_SPI_IntClock_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 PSoC_SPI_IntClock_GetDividerRegister(void);
uint8  PSoC_SPI_IntClock_GetFractionalDividerRegister(void);

#define PSoC_SPI_IntClock_Enable()                         PSoC_SPI_IntClock_Start()
#define PSoC_SPI_IntClock_Disable()                        PSoC_SPI_IntClock_Stop()
#define PSoC_SPI_IntClock_SetDividerRegister(clkDivider, reset)  \
    PSoC_SPI_IntClock_SetFractionalDividerRegister((clkDivider), 0u)
#define PSoC_SPI_IntClock_SetDivider(clkDivider)           PSoC_SPI_IntClock_SetDividerRegister((clkDivider), 1u)
#define PSoC_SPI_IntClock_SetDividerValue(clkDivider)      PSoC_SPI_IntClock_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define PSoC_SPI_IntClock_DIV_ID     PSoC_SPI_IntClock__DIV_ID

#define PSoC_SPI_IntClock_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define PSoC_SPI_IntClock_CTRL_REG   (*(reg32 *)PSoC_SPI_IntClock__CTRL_REGISTER)
#define PSoC_SPI_IntClock_DIV_REG    (*(reg32 *)PSoC_SPI_IntClock__DIV_REGISTER)

#define PSoC_SPI_IntClock_CMD_DIV_SHIFT          (0u)
#define PSoC_SPI_IntClock_CMD_PA_DIV_SHIFT       (8u)
#define PSoC_SPI_IntClock_CMD_DISABLE_SHIFT      (30u)
#define PSoC_SPI_IntClock_CMD_ENABLE_SHIFT       (31u)

#define PSoC_SPI_IntClock_CMD_DISABLE_MASK       ((uint32)((uint32)1u << PSoC_SPI_IntClock_CMD_DISABLE_SHIFT))
#define PSoC_SPI_IntClock_CMD_ENABLE_MASK        ((uint32)((uint32)1u << PSoC_SPI_IntClock_CMD_ENABLE_SHIFT))

#define PSoC_SPI_IntClock_DIV_FRAC_MASK  (0x000000F8u)
#define PSoC_SPI_IntClock_DIV_FRAC_SHIFT (3u)
#define PSoC_SPI_IntClock_DIV_INT_MASK   (0xFFFFFF00u)
#define PSoC_SPI_IntClock_DIV_INT_SHIFT  (8u)

#else 

#define PSoC_SPI_IntClock_DIV_REG        (*(reg32 *)PSoC_SPI_IntClock__REGISTER)
#define PSoC_SPI_IntClock_ENABLE_REG     PSoC_SPI_IntClock_DIV_REG
#define PSoC_SPI_IntClock_DIV_FRAC_MASK  PSoC_SPI_IntClock__FRAC_MASK
#define PSoC_SPI_IntClock_DIV_FRAC_SHIFT (16u)
#define PSoC_SPI_IntClock_DIV_INT_MASK   PSoC_SPI_IntClock__DIVIDER_MASK
#define PSoC_SPI_IntClock_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_PSoC_SPI_IntClock_H) */

/* [] END OF FILE */

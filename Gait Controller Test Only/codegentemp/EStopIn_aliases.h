/*******************************************************************************
* File Name: EStopIn.h  
* Version 2.10
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EStopIn_ALIASES_H) /* Pins EStopIn_ALIASES_H */
#define CY_PINS_EStopIn_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define EStopIn_0		(EStopIn__0__PC)
#define EStopIn_0_PS		(EStopIn__0__PS)
#define EStopIn_0_PC		(EStopIn__0__PC)
#define EStopIn_0_DR		(EStopIn__0__DR)
#define EStopIn_0_SHIFT	(EStopIn__0__SHIFT)


#endif /* End Pins EStopIn_ALIASES_H */


/* [] END OF FILE */

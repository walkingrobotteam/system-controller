/*******************************************************************************
* File Name: EStopIn.h  
* Version 2.10
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EStopIn_H) /* Pins EStopIn_H */
#define CY_PINS_EStopIn_H

#include "cytypes.h"
#include "cyfitter.h"
#include "EStopIn_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    EStopIn_Write(uint8 value) ;
void    EStopIn_SetDriveMode(uint8 mode) ;
uint8   EStopIn_ReadDataReg(void) ;
uint8   EStopIn_Read(void) ;
uint8   EStopIn_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define EStopIn_DRIVE_MODE_BITS        (3)
#define EStopIn_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - EStopIn_DRIVE_MODE_BITS))

#define EStopIn_DM_ALG_HIZ         (0x00u)
#define EStopIn_DM_DIG_HIZ         (0x01u)
#define EStopIn_DM_RES_UP          (0x02u)
#define EStopIn_DM_RES_DWN         (0x03u)
#define EStopIn_DM_OD_LO           (0x04u)
#define EStopIn_DM_OD_HI           (0x05u)
#define EStopIn_DM_STRONG          (0x06u)
#define EStopIn_DM_RES_UPDWN       (0x07u)

/* Digital Port Constants */
#define EStopIn_MASK               EStopIn__MASK
#define EStopIn_SHIFT              EStopIn__SHIFT
#define EStopIn_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define EStopIn_PS                     (* (reg32 *) EStopIn__PS)
/* Port Configuration */
#define EStopIn_PC                     (* (reg32 *) EStopIn__PC)
/* Data Register */
#define EStopIn_DR                     (* (reg32 *) EStopIn__DR)
/* Input Buffer Disable Override */
#define EStopIn_INP_DIS                (* (reg32 *) EStopIn__PC2)


#if defined(EStopIn__INTSTAT)  /* Interrupt Registers */

    #define EStopIn_INTSTAT                (* (reg32 *) EStopIn__INTSTAT)

#endif /* Interrupt Registers */


/***************************************
* The following code is DEPRECATED and 
* must not be used.
***************************************/

#define EStopIn_DRIVE_MODE_SHIFT       (0x00u)
#define EStopIn_DRIVE_MODE_MASK        (0x07u << EStopIn_DRIVE_MODE_SHIFT)


#endif /* End Pins EStopIn_H */


/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "BLEProcess.h"
#include "main.h"

void UpdateRobotCommandcharacteristic(uint8* commandData, uint8 len, uint16 attrHandle)
{
        /* 'rgbHandle' stores RGB control data parameters */
        CYBLE_GATT_HANDLE_VALUE_PAIR_T RobotCommandHandle;
        /* Update RGB control handle with new values */
        RobotCommandHandle.attrHandle = attrHandle;
        RobotCommandHandle.value.val = commandData;
        RobotCommandHandle.value.len = len;
        /* Update the RobotCommand attribute value. This will allow
        * Client device to read the existing color values over
        * Robot Command characteristic */
         CyBle_GattsWriteAttributeValue(&RobotCommandHandle,FALSE,&cyBle_connHandle,CYBLE_GATT_DB_LOCALLY_INITIATED);
}

/* [] END OF FILE */

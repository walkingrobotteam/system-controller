/*******************************************************************************
* File Name: PSoC_SPI_INT.c
* Version 2.70
*
* Description:
*  This file provides all Interrupt Service Routine (ISR) for the SPI Slave
*  component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PSoC_SPI_PVT.h"


/* User code required at start of ISR */
/* `#START PSoC_SPI_ISR_START_DEF` */

/* `#END` */


/*******************************************************************************
* Function Name: PSoC_SPI_TX_ISR
*
* Summary:
*  Interrupt Service Routine for TX portion of the SPI Slave.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  PSoC_SPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer.
*  PSoC_SPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer, modified when exist data to
*  sending and FIFO Not Full.
*  PSoC_SPI_txBuffer[PSoC_SPI_TX_BUFFER_SIZE] - used to store
*  data to sending.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(PSoC_SPI_TX_ISR)
{

    #if(PSoC_SPI_TX_SOFTWARE_BUF_ENABLED)
        uint8 tmpStatus;
    #endif /* (PSoC_SPI_TX_SOFTWARE_BUF_ENABLED) */

    /* User code required at start of ISR */
    /* `#START PSoC_SPI_ISR_TX_START` */

    /* `#END` */

    #if(PSoC_SPI_TX_SOFTWARE_BUF_ENABLED)
        /* Component interrupt service code */

        /* See if TX data buffer is not empty and there is space in TX FIFO */
        while(PSoC_SPI_txBufferRead != PSoC_SPI_txBufferWrite)
        {
            tmpStatus = PSoC_SPI_GET_STATUS_TX(PSoC_SPI_swStatusTx);
            PSoC_SPI_swStatusTx = tmpStatus;

            if ((PSoC_SPI_swStatusTx & PSoC_SPI_STS_TX_FIFO_NOT_FULL) != 0u)
            {
                if(PSoC_SPI_txBufferFull == 0u)
                {
                   PSoC_SPI_txBufferRead++;

                    if(PSoC_SPI_txBufferRead >= PSoC_SPI_TX_BUFFER_SIZE)
                    {
                        PSoC_SPI_txBufferRead = 0u;
                    }
                }
                else
                {
                    PSoC_SPI_txBufferFull = 0u;
                }

                /* Put data element into the TX FIFO */
                CY_SET_REG8(PSoC_SPI_TXDATA_PTR, 
                                             PSoC_SPI_txBuffer[PSoC_SPI_txBufferRead]);
            }
            else
            {
                break;
            }
        }

        /* If Buffer is empty then disable TX FIFO status interrupt until there is data in the buffer */
        if(PSoC_SPI_txBufferRead == PSoC_SPI_txBufferWrite)
        {
            PSoC_SPI_TX_STATUS_MASK_REG &= ((uint8)~PSoC_SPI_STS_TX_FIFO_NOT_FULL);
        }

    #endif /* PSoC_SPI_TX_SOFTWARE_BUF_ENABLED */

    /* User code required at end of ISR (Optional) */
    /* `#START PSoC_SPI_ISR_TX_END` */

    /* `#END` */

   }


/*******************************************************************************
* Function Name: PSoC_SPI_RX_ISR
*
* Summary:
*  Interrupt Service Routine for RX portion of the SPI Slave.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  PSoC_SPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer modified when FIFO contains
*  new data.
*  PSoC_SPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified when overflow occurred.
*  PSoC_SPI_rxBuffer[PSoC_SPI_RX_BUFFER_SIZE] - used to store
*  received data, modified when FIFO contains new data.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(PSoC_SPI_RX_ISR)
{
    #if(PSoC_SPI_RX_SOFTWARE_BUF_ENABLED)
        uint8 tmpStatus;
        uint8 rxData;
    #endif /* (PSoC_SPI_TX_SOFTWARE_BUF_ENABLED) */

    /* User code required at start of ISR */
    /* `#START PSoC_SPI_RX_ISR_START` */

    /* `#END` */

    #if(PSoC_SPI_RX_SOFTWARE_BUF_ENABLED)
        tmpStatus = PSoC_SPI_GET_STATUS_RX(PSoC_SPI_swStatusRx);
        PSoC_SPI_swStatusRx = tmpStatus;
        /* See if RX data FIFO has some data and if it can be moved to the RX Buffer */
        while((PSoC_SPI_swStatusRx & PSoC_SPI_STS_RX_FIFO_NOT_EMPTY) != 0u)
        {
            rxData = CY_GET_REG8(PSoC_SPI_RXDATA_PTR);

            /* Set next pointer. */
            PSoC_SPI_rxBufferWrite++;
            if(PSoC_SPI_rxBufferWrite >= PSoC_SPI_RX_BUFFER_SIZE)
            {
                PSoC_SPI_rxBufferWrite = 0u;
            }

            if(PSoC_SPI_rxBufferWrite == PSoC_SPI_rxBufferRead)
            {
                PSoC_SPI_rxBufferRead++;
                if(PSoC_SPI_rxBufferRead >= PSoC_SPI_RX_BUFFER_SIZE)
                {
                    PSoC_SPI_rxBufferRead = 0u;
                }
                PSoC_SPI_rxBufferFull = 1u;
            }

            /* Move data from the FIFO to the Buffer */
            PSoC_SPI_rxBuffer[PSoC_SPI_rxBufferWrite] = rxData;

            tmpStatus = PSoC_SPI_GET_STATUS_RX(PSoC_SPI_swStatusRx);
            PSoC_SPI_swStatusRx = tmpStatus;
        }
    #endif /* PSoC_SPI_RX_SOFTWARE_BUF_ENABLED */

    /* User code required at end of ISR (Optional) */
    /* `#START PSoC_SPI_RX_ISR_END` */

    /* `#END` */

}

/* [] END OF FILE */

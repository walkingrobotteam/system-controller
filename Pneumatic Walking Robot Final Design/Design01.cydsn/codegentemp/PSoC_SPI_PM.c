/*******************************************************************************
* File Name: PSoC_SPI_PM.c
* Version 2.70
*
* Description:
*  This file contains the setup, control and status commands to support
*  component operations in low power mode.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PSoC_SPI_PVT.h"

static PSoC_SPI_BACKUP_STRUCT PSoC_SPI_backup = 
{
    PSoC_SPI_DISABLED,
    PSoC_SPI_BITCTR_INIT,
};


/*******************************************************************************
* Function Name: PSoC_SPI_SaveConfig
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void PSoC_SPI_SaveConfig(void) 
{

}


/*******************************************************************************
* Function Name: PSoC_SPI_RestoreConfig
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void PSoC_SPI_RestoreConfig(void) 
{

}


/*******************************************************************************
* Function Name: PSoC_SPI_Sleep
********************************************************************************
*
* Summary:
*  Prepare SPI Slave Component goes to sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  PSoC_SPI_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void PSoC_SPI_Sleep(void) 
{
    /* Save components enable state */
    if ((PSoC_SPI_TX_STATUS_ACTL_REG & PSoC_SPI_INT_ENABLE) != 0u)
    {
        PSoC_SPI_backup.enableState = 1u;
    }
    else /* Components block is disabled */
    {
        PSoC_SPI_backup.enableState = 0u;
    }

    PSoC_SPI_Stop();

}


/*******************************************************************************
* Function Name: PSoC_SPI_Wakeup
********************************************************************************
*
* Summary:
*  Prepare SPIM Component to wake up.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  PSoC_SPI_backup - used when non-retention registers are restored.
*  PSoC_SPI_txBufferWrite - modified every function call - resets to
*  zero.
*  PSoC_SPI_txBufferRead - modified every function call - resets to
*  zero.
*  PSoC_SPI_rxBufferWrite - modified every function call - resets to
*  zero.
*  PSoC_SPI_rxBufferRead - modified every function call - resets to
*  zero.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void PSoC_SPI_Wakeup(void) 
{
    #if (PSoC_SPI_TX_SOFTWARE_BUF_ENABLED)
        PSoC_SPI_txBufferFull = 0u;
        PSoC_SPI_txBufferRead = 0u;
        PSoC_SPI_txBufferWrite = 0u;
    #endif /* PSoC_SPI_TX_SOFTWARE_BUF_ENABLED */

    #if (PSoC_SPI_RX_SOFTWARE_BUF_ENABLED)
        PSoC_SPI_rxBufferFull = 0u;
        PSoC_SPI_rxBufferRead = 0u;
        PSoC_SPI_rxBufferWrite = 0u;
    #endif /* PSoC_SPI_RX_SOFTWARE_BUF_ENABLED */

    PSoC_SPI_ClearFIFO();

    /* Restore components block enable state */
    if (PSoC_SPI_backup.enableState != 0u)
    {
         /* Components block was enabled */
         PSoC_SPI_Enable();
    } /* Do nothing if components block was disabled */
}


/* [] END OF FILE */

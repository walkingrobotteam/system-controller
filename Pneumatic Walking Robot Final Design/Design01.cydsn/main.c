/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "BLEProcess.h"
#include "main.h"
//#include "led.h"

enum STATES { init, waitForCommand, sendCommand, reset, stop } systemState;
int8 STOP, RESET, IDLE,commandReceived;
//uint8 CommandData[1]; //LENGTH SHOULD EQUAL . Sending 1
uint8 CommandData; //LENGTH SHOULD EQUAL . Sending 1
uint8 result = 0x00;
void GeneralEventHandler( uint32 eventCode, void *eventParam );
void initialize();
void sending(uint8_t cmd);
void resetSystem();
void stopSystem();
CY_ISR(EmergencyStop);

int main()
{
    CyGlobalIntEnable;   /* Enable global interrupts */
    
    systemState = init;
    
    
    
    for(;;)
    {
        switch(systemState){
            
            
            case init:
            initialize();
            systemState = waitForCommand;
            break;
            
            case waitForCommand:

            while(commandReceived == 0){ //VERSION WILL RUN WITHOUT commandReceive Changing
                CyBle_ProcessEvents(); //RUN BLE STACK
                
        }
                if(commandReceived == 1){ 
                     if(CommandData == 0x15){ //COMMAND = STOP
                        systemState = stop;
                    }
                    if(CommandData == 0x16){ //COMMAND = RESET
                        systemState = reset;
                    }                   
                    systemState = sendCommand;
                }
                
            
            break;
            
            case sendCommand:
                //poll FIFO MUST BE DONE BEFORE TRANSMITTING DATA
                while(result == 0x00){
                    result = PSoC_SPI_ReadRxData(); 
                }
                PSoC_SPI_ClearTxBuffer();//CLEAR SPI TX BUFFER
                PSoC_SPI_ClearRxBuffer(); //CLEAR SPI RX BUFFER
                
                 //send the command to the MEGA
                sending(CommandData);

                //change the command Received back to 0
                commandReceived = 0;
                
                CommandData = 0x00; //RESET CommandData
                
                //wait for another command 
                systemState = waitForCommand; //for Now just go back to waiting for a command
            break;
            
            case reset:
            resetSystem();
            break;
            
            case stop:
            stopSystem();
            break; 
        }
    }
}


void GeneralEventHandler(uint32 event, void * eventParam)
      {
        /* Structure to store data written by Client */
        CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;

            switch(event)
             {
            case CYBLE_EVT_STACK_ON:
            /* This event is generated when BLE stack is ON */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            break;

            
            
            
            case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
                /* This event is generated whenever Advertisement starts or stops */
            if(CyBle_GetState() == CYBLE_STATE_DISCONNECTED)
            {
                CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
                systemState = waitForCommand;
             }
            break;
            
            
            
            
            case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            /* This event is generated at GAP disconnection. */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            systemState = waitForCommand;
            break;
            
            
            
            
             case CYBLE_EVT_GATTS_WRITE_REQ:
            /* This event is generated when the connected Central */
            /* device sends a Write request. */
            /* The parameter ‘eventParam’ contains the data written */
            
            /* Extract the Write data sent by Client */
            wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T *) eventParam;
            
            
            /* If the attribute handle of the characteristic written to * is equal to that of Robot Command characteristic, then extract * command data */ 
            if(CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE == wrReqParam->handleValPair.attrHandle) 
            { 
                commandReceived = 1; // a command is received
                
                

                /* Store Command data in variable */
                CommandData = wrReqParam->handleValPair.value.val[0]; //data stored at index 0

                
                /* Update the GATT DB for Robot Command read characteristics*/
                UpdateRobotCommandcharacteristic(&CommandData, sizeof(CommandData), CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE );
                    
                    if(CommandData == 0x11){ //turn right
                CommandData = 0x11; //NEEDED TO STORE DATA LONG TERM
                    }
                    else if(CommandData == 0x12){ //turn left
                //send the command to the MEGA
                CommandData = 0x12;//NEEDED TO STORE DATA LONG TERM
                    }
                    if(CommandData == 0x13){ //walk forward
                CommandData = 0x13;//NEEDED TO STORE DATA LONG TERM
                    
                    }
                    if(CommandData == 0x14){ //walk backwards
                    CommandData = 0x14;//NEEDED TO STORE DATA LONG TERM
                    }
                    if(CommandData == 0x15){ //STOP
                     CommandData = 0x15;//NEEDED TO STORE DATA LONG TERM
                    }
                    if(CommandData == 0x16){ //RESET
                    CommandData = 0x16;//NEEDED TO STORE DATA LONG TERM
                    }
            }
            
            
            /* Send the response to the write request received. */
            CyBle_GattsWriteRsp(cyBle_connHandle);
            
            
            break;
            
            

            case CYBLE_EVT_GATT_DISCONNECT_IND:
            /* This event is generated at GATT disconnection. */
            
            /* Reset the command data values*/
            CommandData = 0x00;
            
            
            /* Register the new data in GATT DB*/
            UpdateRobotCommandcharacteristic(&CommandData,
            sizeof(CommandData),
            CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE);
            systemState = waitForCommand; //return to command Data
            break;
            
            
             default:
             break;
            }
      }
void initialize(){
    //turn on RED LED
    CyDelay(1500);
    //E_Stop_Start();
    //E_Stop_StartEx(EmergencyStop);
    CyGlobalIntEnable; //enable interrupts
    PSoC_SPI_Start();
    CyBle_Start(GeneralEventHandler);
    
    //reset RESET and STOP
    RESET = 0;
    STOP = 0;
    
    //CyDelay(1500);
    
}

void sending(uint8 cmd){
    
        //get rid of garbage data in tx and rx buffer

            //switch to let Slave transmit
    PSoC_SPI_TxEnable(); 
    
    //write command to tx buffer
    PSoC_SPI_WriteTxDataZero(cmd);

    
    while (0u == (PSoC_SPI_ReadTxStatus() & PSoC_SPI_STS_SPI_DONE )){
            //do nothing
    }
    
    // Clear dummy bytes from RX buffer 
    PSoC_SPI_ClearTxBuffer();
    PSoC_SPI_TxDisable(); //PUT SPI back into RX mode

}

void resetSystem(){
    //CyGlobalIntEnable; //enable interrupts
    CyBle_SoftReset();
    systemState = init;
}
void stopSystem(){
    CyGlobalIntDisable; //Disable interrupts
    CyBle_Stop();
    //turn off LEDs   
}

CY_ISR(EmergencyStop){
    //return pin to 0
    EStopIn_Write(0);
    //set STOP flag to 1
    STOP = 1;
    
    systemState = stop;//move state to STOP state
}



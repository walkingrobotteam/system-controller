/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "BLEProcess.h"
#include "main.h"

enum STATES { init, waitForCommand, sendCommand, reset, stop } systemState;
int8 STOP, RESET, IDLE,commandReceived;
uint8 CommandData = 0x00; //LENGTH SHOULD EQUAL . Sending 1
uint8 result = 0x00;
void GeneralEventHandler( uint32 eventCode, void *eventParam );
void initialize();
void sending(uint8_t cmd);
void resetSystem();
void stopSystem();
//CY_ISR(EmergencyStop);

int main()
{
    CyGlobalIntEnable;   /* Enable global interrupts */
    
    systemState = init;
    
    
    
    for(;;)
    {
        /* Place your application code here */
        //CyBle_ProcessEvents();
        switch(systemState){
            case init:
            initialize();
            systemState = waitForCommand;
            break;
            
            case waitForCommand:
            while(commandReceived == 0){
            CyBle_ProcessEvents();
                PSoC_SPI_ClearTxBuffer();
                PSoC_SPI_ClearRxBuffer();
        
                //poll FIFO 
                while(result == 0x00){
                    result = PSoC_SPI_ReadRxData(); 
                }
                
                //send the command to the MEGA
                //CommandData = 0x13;
                sending(CommandData);
            if(STOP == 1){ //check first
                systemState = stop; 
                break;
            }
            else if(RESET == 1){ //chec 
                systemState = reset;
                break;
            }
        }
               /* if(commandReceived == 1){
                    systemState = sendCommand;
                }*/ //do not need already done in ble stack
                
            systemState = waitForCommand;
            break;
            
            case sendCommand:
                //should check to see whether command is stop or reset,
                //if stop or reset, then set STOP = 1 or RESET = 1 respectively
                //if stop or reset immediately move into reset or stop states
           
                //do stuff first
                PSoC_SPI_ClearTxBuffer();
                PSoC_SPI_ClearRxBuffer();
        
                //poll FIFO 
                while(result == 0x00){
                    result = PSoC_SPI_ReadRxData(); 
                }
                
                //send the command to the MEGA
                CommandData = 0x13;
                sending(CommandData);
                PSoC_SPI_ClearTxBuffer();
                PSoC_SPI_ClearRxBuffer();
                
            if(STOP == 1){ //check first
                systemState = stop; 
                break;
            }
            else if(RESET == 1){ //chec 
                systemState = reset;
                break;
            }
                //change the command Received back to 0
                commandReceived = 0;
                //wait for another command 
                systemState = waitForCommand; //for Now just go back to waiting for a command
            break;
            
            case reset:
            resetSystem();
            break;
            
            case stop:
            //send stop command
            sending(0x15);
            stopSystem();
            break; 
        }
    }
}



void GeneralEventHandler(uint32 event, void * eventParam)
      {
        /* Structure to store data written by Client */
        CYBLE_GATTS_WRITE_REQ_PARAM_T *wrReqParam;
        /* 'CommandData' is a variable to store 1 byte of Command data*/
            switch(event)
             {
            case CYBLE_EVT_STACK_ON:
            /* This event is generated when BLE stack is ON */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            break;

            
            
            
            case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
                /* This event is generated whenever Advertisement starts or stops */
            if(CyBle_GetState() == CYBLE_STATE_DISCONNECTED)
            {
                CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
             }
            break;
            
            
            
            
            case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            /* This event is generated at GAP disconnection. */
            CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            systemState = waitForCommand;
            break;
            
            
            
            
             case CYBLE_EVT_GATTS_WRITE_REQ:
            /* This event is generated when the connected Central */
            /* device sends a Write request. */
            /* The parameter ‘eventParam’ contains the data written */
            
            /* Extract the Write data sent by Client */
            wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T *) eventParam;
            
            
            /* If the attribute handle of the characteristic written to * is equal to that of Robot Command characteristic, then extract * command data */ 
            if(CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE == wrReqParam->handleValPair.attrHandle) 
            { 
                //commandReceived = 1; // a command is received
                
                
                //should store DATA into command variable.
                //after storing data move state to send command state
                CommandData = wrReqParam->handleValPair.value.val[0]; //data stored at index 0

                
                /* Update the GATT DB for Robot Command read characteristics*/
                UpdateRobotCommandcharacteristic(&CommandData, sizeof(CommandData), CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE );
                
                    if(CommandData == 0x11){ //BLUE  //turn right
                    //CyDelay(2000);
                    }
                    if(CommandData == 0x12){ //RED //turn left
 
                    //CyDelay(2000);
                    }
                    if(CommandData == 0x13){ //GREEN/BLUE/RED  //walk forward
                        
                    PSoC_SPI_ClearTxBuffer();
                    PSoC_SPI_ClearRxBuffer();
        
                 //poll FIFO 
                     while(result == 0x00){
                    result = PSoC_SPI_ReadRxData(); 
                      }
                    sending(0x13);
                                        //CyDelay(2000);
                     }
                    if(CommandData == 0x14){ //PURPLE walk backwards
                    }
                    if(CommandData == 0x15){ // GREEN& BLUE STOP
                    }
                    if(CommandData == 0x16){ // GREEN& BLUE RESET
   
                    }
                    //systemState = sendCommand;
                    //break;
            }
            
            
            /* Send the response to the write request received. */
            CyBle_GattsWriteRsp(cyBle_connHandle);
            
            
            break;
            
            

            case CYBLE_EVT_GATT_DISCONNECT_IND:
            /* This event is generated at GATT disconnection. */
            
            /* Reset the command value*/
            //CommandData = FALSE; //change value
                /*PrISM_1_WritePulse0(0X00);
                PrISM_1_WritePulse1(0x00);
                PrISM_2_WritePulse0(0X00);*/
            
            
            /* Switch off LEDs */
            //UpdateRGBLED(RGBledData, sizeof(RGBledData));
            //SHOW WAITING FOR CONNECTION ON LED's
            
            /* Register the new color in GATT DB*/
            UpdateRobotCommandcharacteristic(&CommandData,
            sizeof(CommandData),
            CYBLE_ROBOT_COMMAND_ROBOT_COMMAND_RXTX_CHAR_HANDLE);
            systemState = waitForCommand;//if no command then go back to waiting
            break;
            
            
             default:
             break;
            }
      }
void initialize(){
    
    CyGlobalIntEnable; //enable interrupts
    E_Stop_Start();
    //E_Stop_StartEx(EmergencyStop);
    
    //start SPI component
    PSoC_SPI_Start();
    
    //Start BLE Stack
    CyBle_Start(GeneralEventHandler);
    
    //initialize RESET and STOP
    RESET = 0;
    STOP = 0;
    
    
}

void resetSystem(){
    //CyGlobalIntEnable; //enable interrupts
    CyBle_SoftReset();
    systemState = init;
}
void stopSystem(){
    CyGlobalIntDisable; //Disable interrupts
    CyBle_Stop();
    //turn off LEDs
    /*PrISM_1_WritePulse0(RGB_LED_OFF);
    PrISM_1_WritePulse1(RGB_LED_OFF);
    PrISM_2_WritePulse0(RGB_LED_OFF);*/
    
}
void sending(uint8 cmd){
    
        //get rid of garbage data in tx and rx buffer

            //switch to let Slave transmit
    PSoC_SPI_TxEnable(); 
    
    //write command to tx buffer
    PSoC_SPI_WriteTxDataZero(cmd);

    
    //read tx status and wait until transmit is complete
    /*IntMega_Write(1); //generate interrupt on MEGA THAT TELLS IT TO START LISTENING FOR A COMMAND
    CyDelay(200);
    IntMega_Write(0); //TURN OFF
    RGB_LED_ON_BLUE;*/
    
    while (0u == (PSoC_SPI_ReadTxStatus() & PSoC_SPI_STS_SPI_DONE )){
            //do nothing
    }
    
    // Clear dummy bytes from RX buffer 
    PSoC_SPI_ClearTxBuffer();
    PSoC_SPI_TxDisable(); //PUT SPI back into RX mode

}


/*CY_ISR(EmergencyStop){
    //return pin to 0
    EStopIn_Write(1);
    //set STOP flag to 1
    STOP = 1;
    //move state to STOP state
    systemState = stop;
}*/
